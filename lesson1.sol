//add two functions to change address and salary
//2017.10.20 by Shunda Lin

pragma solidity ^0.4.14;

contract Payroll {
    uint salary = 1 ether;
    address user = 0x4b0897b0513fdc7c541b6d9d7e929c4e5364d2db;
    uint payDuration = 10 seconds;
    uint lastPayday = now;
    
    function addFund() payable returns (uint){
        return this.balance;
    }
    
    function calculateRunway() returns (uint) {
        return this.balance/salary;
    }
    
    function hasEnoughFund() returns (bool) {
        return calculateRunway() > 0;
    }
    
    function getPaid() {
        if (msg.sender != user) {
            revert();
        }
        
        uint nextPayDay = lastPayday + payDuration;
        if (nextPayDay > now) {
            revert();
        }
        
        lastPayday = nextPayDay;
        user.transfer(salary);
    }
    
    function changeAddr(address addr) returns (address) {
        user = addr;
        return user;
    }
    
    function changeSalary(uint newSalary) returns (uint) {
        salary = newSalary * 1 ether;
        return salary;
    }
    
    function getUser() returns (address) {
        return user;
    }
    
    function getSalary() returns (uint) {
        return salary;
    }
}